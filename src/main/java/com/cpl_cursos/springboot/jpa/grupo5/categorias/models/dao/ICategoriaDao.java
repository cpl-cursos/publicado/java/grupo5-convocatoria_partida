package com.cpl_cursos.springboot.jpa.grupo5.categorias.models.dao;

import com.cpl_cursos.springboot.jpa.grupo5.categorias.models.entity.Categoria;

import java.util.List;

public interface ICategoriaDao {
    List<Categoria> findAll();
    void save(Categoria categoria);  // recibe el objeto mapeado a la tabla dentro del contexto de persistencia
    Categoria findOne(Long id);
    void delete(Long id);
}
