package com.cpl_cursos.springboot.jpa.grupo5.categorias.controllers;

import com.cpl_cursos.springboot.jpa.grupo5.categorias.models.dao.ICategoriaDao;
import com.cpl_cursos.springboot.jpa.grupo5.categorias.models.entity.Categoria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.Banner;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
public class CategoriaController {

    @Autowired      // busca autom. un componente registrado en el contenedor -un beans- que implemente esta interfaz
    @Qualifier("clienteDaoJPA")      // puede haber varias implementaciones del dao. Se puede omitir pues sólo hay una.
    private ICategoriaDao categoriaDao;

    @GetMapping(value = "/listar")
    public String listar(Model modelo) {
        modelo.addAttribute("titulo", "Listado de Categorías");
        modelo.addAttribute("categorias", categoriaDao.findAll()); // para la lista de categorías
        Categoria cat = new Categoria();  // para usar con el formulario del botón de añadir categoría
        modelo.addAttribute("cat", cat);  // para poder incluir el formulario en la misma pantalla del listado
        return "lista_categorias";
    }

    @PostMapping(value = "/nuevacat")
    public String crear(Categoria cat) {
        categoriaDao.save(cat);
        return "redirect:listar";
    }

    @PostMapping(value = "/editacat")
    public String editar(@RequestBody Categoria cat, Model categ) {
        categ.addAttribute("categoria", cat);
        categ.addAttribute("titulo_modal", "Modificar categoría");
        return "editCatModal";
    }

    @PostMapping(value="/cambiacat")
    public String cambiar(Categoria cat) {
        categoriaDao.save(cat);
        return "redirect:listar";
    }

    @GetMapping(value = "/borrar/{id}")
    public String borrar(@PathVariable(value = "id") Long id) {
        if (id > 0) {
            categoriaDao.delete(id);
        }
        return "redirect:/listar";
    }

    @GetMapping("/select")
    public String select(Model modelo) {
        modelo.addAttribute("titulo", "Listado de Categorías");
        modelo.addAttribute("categorias", categoriaDao.findAll()); // para la lista de categorías
        Categoria cat = new Categoria();  // Se necesita para el formulario
        modelo.addAttribute("cat", cat);
        return "select";
    }

    @PostMapping(path="/select")
    public String confirma(Categoria cat, Model categ) {
        categ.addAttribute("categoria2", cat.getIdCategoria()  );
        categ.addAttribute("titulo2", "Recibido en el servidor");
        categ.addAttribute("titulo", "Listado de Categorías");
        categ.addAttribute("categorias", categoriaDao.findAll());
        Categoria cat1 = new Categoria();  // Se necesita para el formulario
        categ.addAttribute("cat", cat1);
        return "select";
    }
}
