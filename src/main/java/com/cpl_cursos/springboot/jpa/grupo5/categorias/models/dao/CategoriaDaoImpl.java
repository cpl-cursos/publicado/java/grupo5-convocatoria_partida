package com.cpl_cursos.springboot.jpa.grupo5.categorias.models.dao;

import com.cpl_cursos.springboot.jpa.grupo5.categorias.models.entity.Categoria;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

// @Repository Indica a String que es un componente persistente en BD.
// Lo identificamos para reconocerlo en el controlador
@Repository("clienteDaoJPA")
public class CategoriaDaoImpl implements ICategoriaDao {

    @PersistenceContext         // Inyecta la config. de la unidad de persistencia JPA (por defecto es H2)
    private EntityManager em;   // se encarga de las operaciones de la BBDD pero con los objetos

    @Override
    @Transactional(readOnly = true)
    public List<Categoria> findAll() {
        return em.createQuery("from Categoria").getResultList();
    }

    @Override
    @Transactional
    public void save(Categoria categoria) {
        if (categoria.getIdCategoria() != null && categoria.getIdCategoria() > 0) {
            em.merge(categoria);
        } else {
            em.persist(categoria);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Categoria findOne(Long id) {
        return em.find(Categoria.class, id);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        Categoria cat = findOne((id));
        em.remove(cat);   // las dos líneas se pueden poner en una como em.remove(findOne(id))
    }
}
